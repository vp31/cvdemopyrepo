import numpy as np
import cv2
import gc

#Assign Global Values
workDir = "/home/vinayak/imgs/"

# Load an image
rawImgName = "Lenna.png"
rawImgPath = workDir + rawImgName
rawImg = cv2.imread(rawImgPath)

print("**** raw img: ****")
print(rawImg)

cv2.imshow("raw img",rawImg)
cv2.waitKey(0)
cv2.destroyAllWindows()

#release memory
del rawImg
gc.collect()