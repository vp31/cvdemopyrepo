import cv2
import gc

#Set Global Values
workDir = "/home/vinayak/imgs/"

# Load an color image in grayscale
imgName = "Lenna.png"
imgPath = workDir + imgName
img = cv2.imread(imgPath ,cv2.IMREAD_GRAYSCALE)

print("**** gray img: ****")
print(img)

cv2.imshow('gray img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

outImgName = "Lenna_Gray.png"
outImgPath = workDir + outImgName
cv2.imwrite(outImgPath,img)

#release memory
del img
gc.collect()